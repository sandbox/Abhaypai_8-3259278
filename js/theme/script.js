/**
 * All theme related interactions.
 */
(function($, Drupal) {
  'use strict';

  Drupal.theme.bs_5_corporate_ToolTip = function () {
    console.log('bs_5_corporate_ToolTip');

    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
      return new bootstrap.Tooltip(tooltipTriggerEl)
    });
  };

})(jQuery, Drupal);
