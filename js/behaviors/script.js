/**
 * All theme related behaviors.
 */
(function($, Drupal, drupalSettings) {
  'use strict';

  Drupal.behaviors.bs_5_corporate = {
    // Entry point of bs_5_corporate behavior.
    attach: function (context) {
      // Jquery object.
      console.log($);
      // Drupal object.
      console.log(Drupal);
      // Drupal object of settings.
      console.log(drupalSettings);
      // DOM.
      console.log(context);

      // Calling theme function.
      Drupal.theme('bs_5_corporate_ToolTip');

      // Custom Behaviours.
      this.clickCallback('#block-aboutus');
    },
    clickCallback: function(selector) {
      $(selector).on('click', function(event) {
        Drupal.behaviors.bs_5_corporate.ajaxCall('https://jsonplaceholder.typicode.com/posts');
      });
    },
    ajaxCall: function(url) {
      $.get(url, function(data, status) {
        Drupal.behaviors.bs_5_corporate._ajaxCallback(data, status);
      });
    },
    _ajaxCallback: function(data, status) {
      console.log(data);
      console.log(status);
    },
  };

})(jQuery, Drupal, drupalSettings);
