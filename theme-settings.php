<?php

/**
 * @file
 * Functions to support BS 5 Corporate theme settings.
 */

use Drupal\Core\Form;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_form_FORM_ID_alter() for system_theme_settings.
 */
function bs_5_corporate_form_system_theme_settings_alter(&$form, FormStateInterface $form_state) {
  $form['bs_5_corporate_settings'] = [
    '#type' => 'vertical_tabs',
    '#title' => new TranslatableMarkup('BS 5 Corporate Settings'),
  ];

  $form['basic'] = [
    '#type' => 'details',
    '#group' => 'bs_5_corporate_settings',
    '#title' => new TranslatableMarkup('Basic'),
  ];

  $form['basic']['type_theme'] = [
    '#type' => 'select',
    '#title' => new TranslatableMarkup('Select Theme.'),
    '#default_value' => theme_get_setting('type_theme'),
    '#options' => [
      'light' => new TranslatableMarkup('Light Version'),
      'dark' => new TranslatableMarkup('Dark Version'),
    ],
  ];

  $form['footer'] = [
    '#type' => 'details',
    '#group' => 'bs_5_corporate_settings',
    '#title' => new TranslatableMarkup('Footer'),
  ];

  $form['footer']['type_footer'] = [
    '#type' => 'select',
    '#title' => new TranslatableMarkup('Select Footer Column.'),
    '#default_value' => theme_get_setting('type_footer'),
    '#options' => [
      '3' => new TranslatableMarkup('4 Column'),
      '4' => new TranslatableMarkup('3 Column'),
      '6' => new TranslatableMarkup('2 Column'),
      '12' => new TranslatableMarkup('1 Column'),
    ],
  ];

  $form['footer']['type_footer_visibility'] = [
    '#type' => 'select',
    '#title' => new TranslatableMarkup('Select Footer Column Visibility.'),
    '#default_value' => theme_get_setting('type_footer_visibility'),
    '#options' => [
      '1' => new TranslatableMarkup('First 1'),
      '2' => new TranslatableMarkup('First 2'),
      '3' => new TranslatableMarkup('First 3'),
      '4' => new TranslatableMarkup('All'),
    ],
  ];

  $form['footer']['type_footer_disable'] = [
    '#type' => 'checkbox',
    '#title' => new TranslatableMarkup('Disable Footer.'),
    '#default_value' => theme_get_setting('type_footer_disable'),
  ];

  $form['content'] = [
    '#type' => 'details',
    '#group' => 'bs_5_corporate_settings',
    '#title' => new TranslatableMarkup('Content'),
  ];

  $form['content']['401_content'] = [
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#title' => new TranslatableMarkup('401 Content.'),
    '#default_value' => theme_get_setting('401_content')['value'],
  ];

  $form['content']['403_content'] = [
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#title' => new TranslatableMarkup('403 Content.'),
    '#default_value' => theme_get_setting('403_content')['value'],
  ];

  $form['content']['content_404'] = [
    '#type' => 'text_format',
    '#format' => 'full_html',
    '#title' => new TranslatableMarkup('404 Content.'),
    '#default_value' => theme_get_setting('404_content')['value'],
  ];

  $form['social'] = [
    '#type' => 'details',
    '#group' => 'bs_5_corporate_settings',
    '#title' => new TranslatableMarkup('Social'),
  ];

  $form['social']['facebook_accnt'] = [
    '#type' => 'textfield',
    '#title' => new TranslatableMarkup('Facebook Account.'),
    '#default_value' => theme_get_setting('facebook_accnt'),
  ];

  $form['social']['facebook_accnt_title'] = [
    '#type' => 'textfield',
    '#title' => new TranslatableMarkup('Facebook Account Title.'),
    '#default_value' => theme_get_setting('facebook_accnt_title'),
  ];

  $form['social']['linkedin_accnt'] = [
    '#type' => 'textfield',
    '#title' => new TranslatableMarkup('Linkedin Account.'),
    '#default_value' => theme_get_setting('linkedin_accnt'),
  ];

  $form['social']['linkedin_accnt_title'] = [
    '#type' => 'textfield',
    '#title' => new TranslatableMarkup('Linkedin Account Title.'),
    '#default_value' => theme_get_setting('linkedin_accnt_title'),
  ];

  $form['social']['twitter_accnt'] = [
    '#type' => 'textfield',
    '#title' => new TranslatableMarkup('Twitter Account.'),
    '#default_value' => theme_get_setting('twitter_accnt'),
  ];

  $form['social']['twitter_accnt_title'] = [
    '#type' => 'textfield',
    '#title' => new TranslatableMarkup('Twitter Account Title.'),
    '#default_value' => theme_get_setting('twitter_accnt_title'),
  ];
}
